import numpy as np
from HiggsBounds.HiggsBounds_Wrapper import HS_mixed


nHzero = 1
nHplus = 1
nPoints = 2

hs = HS_mixed(nHzero, nHplus, "LandH", nPoints)

hs.Mh = np.full_like(hs.Mh, 125.)
hs.GammaTotal = np.full((nPoints, nHzero), 1.0)
hs.CP_value = np.full((nPoints, nHzero), 0)

hs.dMhneut = np.full((nPoints, nHzero), 3.0)
hs.dMhch = np.full((nPoints, nHplus), 3.0)

hs.ghjss_s = np.full((nPoints, nHzero), 0.1)
hs.ghjss_p = np.full((nPoints, nHzero), 0)
hs.ghjcc_s = np.full((nPoints, nHzero), 0.1)
hs.ghjcc_p = np.full((nPoints, nHzero), 0)
hs.ghjbb_s = np.full((nPoints, nHzero), 0.99)
hs.ghjbb_p = np.full((nPoints, nHzero), 0)
hs.ghjtt_s = np.full((nPoints, nHzero), 0.99)
hs.ghjtt_p = np.full((nPoints, nHzero), 0)
hs.ghjmumu_s = np.full((nPoints, nHzero), 0.9)
hs.ghjmumu_p = np.full((nPoints, nHzero), 0)
hs.ghjtautau_s = np.full((nPoints, nHzero), 0.9)
hs.ghjtautau_p = np.full((nPoints, nHzero), 0)
hs.ghjWW = np.full((nPoints, nHzero), 1)
hs.ghjZZ = np.full((nPoints, nHzero), 1)
hs.ghjZga = np.full((nPoints, nHzero), 0)
hs.ghjgaga = np.full((nPoints, nHzero), 1.0)
hs.ghjgg = np.full((nPoints, nHzero), 1.00)
hs.ghjhiZ = np.full((nPoints, nHzero, nHzero), 1)

hs.BR_hjss = np.full((nPoints, nHzero), 0)
hs.BR_hjcc = np.full((nPoints, nHzero), 0)
hs.BR_hjbb = np.full((nPoints, nHzero), 0.48)
hs.BR_hjtt = np.full((nPoints, nHzero), 0.0)
hs.BR_hjmumu = np.full((nPoints, nHzero), 0)
hs.BR_hjtautau = np.full((nPoints, nHzero), 0.06)
hs.BR_hjWW = np.full((nPoints, nHzero), 0.22)
hs.BR_hjZZ = np.full((nPoints, nHzero), 0.02)
hs.BR_hjZga = np.full((nPoints, nHzero), 0)
hs.BR_hjgaga = np.full((nPoints, nHzero), 0.0022)
hs.BR_hjgg = np.full((nPoints, nHzero), 0.1)

hs.BR_hjinvisible = np.full((nPoints, nHzero), 0)
hs.BR_hkhjhi = np.full((nPoints, nHzero, nHzero, nHzero), 0)
hs.BR_hjhiZ = np.full((nPoints, nHzero, nHzero), 0)
hs.BR_hjemu = np.full((nPoints, nHzero), 0)
hs.BR_hjetau = np.full((nPoints, nHzero), 0)
hs.BR_hjmutau = np.full((nPoints, nHzero), 0)
hs.BR_hjHpiW = np.full((nPoints, nHzero, nHplus), 0)

hs.Mhplus = np.full((nPoints, nHplus), 500.)
hs.GammaTotal_Hpj = np.full((nPoints, nHplus), 1.)
hs.CS_lep_HpjHmj_ratio = np.full((nPoints, nHplus), 1.)
hs.BR_tWpb = np.full((nPoints), 1.)
hs.BR_tHpjb = np.full((nPoints, nHplus), 0.)
hs.BR_Hpjcs = np.full((nPoints, nHplus), 0.)
hs.BR_Hpjcb = np.full((nPoints, nHplus), 0.)
hs.BR_Hpjtaunu = np.full((nPoints, nHplus), 0.0)
hs.BR_Hpjtb = np.full((nPoints, nHplus), 1.0)
hs.BR_HpjWZ = np.full((nPoints, nHplus), 0.0)
hs.BR_HpjhiW = np.full((nPoints, nHplus, nHzero), 0.)

hs.pdf = np.full((nPoints), 2)
hs.nparam = np.full((nPoints), 1)

hs.run_hs_full()
print(hs.Delta_Chisq)
print(hs.ggH)
print(hs.nobs)
