import numpy as np
from HiggsBounds.HiggsBounds_Wrapper import HB_mixed


nHzero = 1
nHplus = 7
nPoints = 2

hb = HB_mixed(nHzero, nHplus, "LandH", nPoints)

# hb.Mh = np.array([[125., 200., 300.],
#                   [125., 400., 600.]])
hb.Mh = np.full_like(hb.Mh, 125.)
hb.GammaTotal = np.full((nPoints, nHzero), 1.0)
hb.CP_value = np.full((nPoints, nHzero), 0)

hb.dMhneut = np.full((nPoints, nHzero), 3.0)
hb.dMhch = np.full((nPoints, nHplus), 3.0)

hb.ghjss_s = np.full((nPoints, nHzero), 0.1)
hb.ghjss_p = np.full((nPoints, nHzero), 0)
hb.ghjcc_s = np.full((nPoints, nHzero), 0.1)
hb.ghjcc_p = np.full((nPoints, nHzero), 0)
hb.ghjbb_s = np.full((nPoints, nHzero), 0.5)
hb.ghjbb_p = np.full((nPoints, nHzero), 0)
hb.ghjtt_s = np.full((nPoints, nHzero), 0.1)
hb.ghjtt_p = np.full((nPoints, nHzero), 0)
hb.ghjmumu_s = np.full((nPoints, nHzero), 0.1)
hb.ghjmumu_p = np.full((nPoints, nHzero), 0)
hb.ghjtautau_s = np.full((nPoints, nHzero), 0.1)
hb.ghjtautau_p = np.full((nPoints, nHzero), 0)
hb.ghjWW = np.full((nPoints, nHzero), 1)
hb.ghjZZ = np.full((nPoints, nHzero), 0)
hb.ghjZga = np.full((nPoints, nHzero), 0)
hb.ghjgaga = np.full((nPoints, nHzero), 1.0)
hb.ghjgg = np.full((nPoints, nHzero), 1.001)
hb.ghjhiZ = np.full((nPoints, nHzero, nHzero), 1)

hb.BR_hjss = np.full((nPoints, nHzero), 0)
hb.BR_hjcc = np.full((nPoints, nHzero), 0)
hb.BR_hjbb = np.full((nPoints, nHzero), 0.48)
hb.BR_hjtt = np.full((nPoints, nHzero), 0.0)
hb.BR_hjmumu = np.full((nPoints, nHzero), 0)
hb.BR_hjtautau = np.full((nPoints, nHzero), 0.04)
hb.BR_hjWW = np.full((nPoints, nHzero), 0.2)
hb.BR_hjZZ = np.full((nPoints, nHzero), 0.02)
hb.BR_hjZga = np.full((nPoints, nHzero), 0)
hb.BR_hjgaga = np.full((nPoints, nHzero), 0.002)
hb.BR_hjgg = np.full((nPoints, nHzero), 0.1)

hb.BR_hjinvisible = np.full((nPoints, nHzero), 0)
hb.BR_hkhjhi = np.full((nPoints, nHzero, nHzero, nHzero), 0)
hb.BR_hjhiZ = np.full((nPoints, nHzero, nHzero), 0)
hb.BR_hjemu = np.full((nPoints, nHzero), 0)
hb.BR_hjetau = np.full((nPoints, nHzero), 0)
hb.BR_hjmutau = np.full((nPoints, nHzero), 0)
hb.BR_hjHpiW = np.full((nPoints, nHzero, nHplus), 0)

hb.Mhplus = np.full((nPoints, nHplus), 500.)
hb.GammaTotal_Hpj = np.full((nPoints, nHplus), 1.)
hb.CS_lep_HpjHmj_ratio = np.full((nPoints, nHplus), 1.)
hb.BR_tWpb = np.full((nPoints), 1.)
hb.BR_tHpjb = np.full((nPoints, nHplus), 0.)
hb.BR_Hpjcs = np.full((nPoints, nHplus), 0.)
hb.BR_Hpjcb = np.full((nPoints, nHplus), 0.)
hb.BR_Hpjtaunu = np.full((nPoints, nHplus), 0.0)
hb.BR_Hpjtb = np.full((nPoints, nHplus), 1.0)
hb.BR_HpjWZ = np.full((nPoints, nHplus), 0.0)
hb.BR_HpjhiW = np.full((nPoints, nHplus, nHzero), 0.)

hb.CS_Hpjtb = np.full((nPoints, nHplus), 0.1)
hb.CS_Hpjcb = np.full((nPoints, nHplus), 0.0)
hb.CS_Hpjbjet = np.full((nPoints, nHplus), 0.0)
hb.CS_Hpjcjet = np.full((nPoints, nHplus), 0.0)
hb.CS_Hpjjetjet = np.full((nPoints, nHplus), 0.0)
hb.CS_HpjW = np.full((nPoints, nHplus), 0.0)
hb.CS_HpjZ = np.full((nPoints, nHplus), 0.0)
hb.CS_vbf_Hpj = np.full((nPoints, nHplus), 0.0)
hb.CS_HpjHmj = np.full((nPoints, nHplus), 0.0)
hb.CS_Hpjhi = np.full((nPoints, nHplus, nHzero), 0.0)

hb.pdf = np.full((nPoints), 2)
hb.nparam = np.full((nPoints), 1)

hb.run_hbhs_full()
print(hb.obsratio_full)
# print(hb.ggH)
print(hb.Delta_Chisq)
# print(hb.ggH)
