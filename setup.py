import os
from setuptools import setup
from setuptools.command.install import install
from distutils.command.install import install as _install

HIGGSBOUNDS_LIB_PATH = \
    "/home/biek/Documents_Local/Tools/higgsbounds/build/lib"
HIGGSSIGNALS_LIB_PATH = \
    "/home/biek/Documents_Local/Tools/higgssignals/build/lib"

with open("README.md", "r") as fh:
    long_description = fh.read()


class install_(install):

    def run(self):
        ret = None
        # Create makefile with HB path inserted
        fin = open("HiggsBounds/Makefile.in", "r")
        fou = open("HiggsBounds/Makefile", "w")
        i1 = 0
        for el in fin:
            if i1 == 0:
                ll = el.replace("XXX", HIGGSBOUNDS_LIB_PATH)
                i1 += 1
            elif i1 == 1:
                ll = el.replace("YYY", HIGGSSIGNALS_LIB_PATH)
                i1 += 1
            else:
                ll = el
            fou.write(ll)
        fin.close()
        fou.close()
        # Make fortran stuff
        os.system("cd HiggsBounds && make HiggsBounds")
        ret = _install.run(self)
        return ret


setup(
    name="HiggsBounds",
    version="0.1.0",
    description="HiggsBounds wrapper to use it in python.",
    long_description=long_description,
    packages=["HiggsBounds"],
    author=["Thomas Biekotter"],
    author_email=["thomas.biekoetter@desy.de"],
    url="https://gitlab.com/thomas.biekoetter/higgsbounds_python_wrapper",
    include_package_data=True,
    package_data={"HiggsBounds": ["*"]},
    install_requires=["numpy", "StandardModel"],
    cmdclass={"install": install_},
    python_requires='>=3.6',
)
