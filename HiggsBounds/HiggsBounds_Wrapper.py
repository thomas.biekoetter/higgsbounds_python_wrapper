import numpy as np
from StandardModel import Higgs

from .HSSMhadr import hssmhadr
from .HBmixed import hbmixed
from .HBHSmixed import hbhsmixed
from .HSmixed import hsmixed



class HB:

    def __init__(self, nHzero, nHplus, mode, nPoints):
        # General
        self.nHzero = nHzero
        self.nHplus = nHplus
        if mode == "LandH":
            self.mode = 0
        self.nPoints = nPoints
        # HiggsBounds_neutral_input_properties
        self.Mh = np.full((nPoints, nHzero), None)
        self.GammaTotal = np.full((nPoints, nHzero), None)
        self.CP_value = np.full((nPoints, nHzero), None)
        # HiggsBounds_set_mass_uncertainties
        self.dMhneut = np.full((nPoints, nHzero), None)
        self.dMhch = np.full((nPoints, nHplus), None)


class HB_mixed(HB):

    def __init__(self, nHzero, nHplus, mode, nPoints):
        super().__init__(nHzero, nHplus, mode, nPoints)
        # For HiggsSignals
        self.pdf = np.full((nPoints), None)
        self.nparam = np.full((nPoints), None)
        # HiggsBounds_neutral_input_effC
        self.ghjss_s = np.full((nPoints, nHzero), None)
        self.ghjss_p = np.full((nPoints, nHzero), None)
        self.ghjcc_s = np.full((nPoints, nHzero), None)
        self.ghjcc_p = np.full((nPoints, nHzero), None)
        self.ghjbb_s = np.full((nPoints, nHzero), None)
        self.ghjbb_p = np.full((nPoints, nHzero), None)
        self.ghjtt_s = np.full((nPoints, nHzero), None)
        self.ghjtt_p = np.full((nPoints, nHzero), None)
        self.ghjmumu_s = np.full((nPoints, nHzero), None)
        self.ghjmumu_p = np.full((nPoints, nHzero), None)
        self.ghjtautau_s = np.full((nPoints, nHzero), None)
        self.ghjtautau_p = np.full((nPoints, nHzero), None)
        self.ghjWW = np.full((nPoints, nHzero), None)
        self.ghjZZ = np.full((nPoints, nHzero), None)
        self.ghjZga = np.full((nPoints, nHzero), None)
        self.ghjgaga = np.full((nPoints, nHzero), None)
        self.ghjgg = np.full((nPoints, nHzero), None)
        self.ghjhiZ = np.full((nPoints, nHzero, nHzero), None)
        # HiggsBounds_neutral_input_SMBR
        self.BR_hjss = np.full((nPoints, nHzero), None)
        self.BR_hjcc = np.full((nPoints, nHzero), None)
        self.BR_hjbb = np.full((nPoints, nHzero), None)
        self.BR_hjtt = np.full((nPoints, nHzero), None)
        self.BR_hjmumu = np.full((nPoints, nHzero), None)
        self.BR_hjtautau = np.full((nPoints, nHzero), None)
        self.BR_hjWW = np.full((nPoints, nHzero), None)
        self.BR_hjZZ = np.full((nPoints, nHzero), None)
        self.BR_hjZga = np.full((nPoints, nHzero), None)
        self.BR_hjgaga = np.full((nPoints, nHzero), None)
        self.BR_hjgg = np.full((nPoints, nHzero), None)
        # HiggsBounds_neutral_input_nonSMBR
        self.BR_hjinvisible = np.full((nPoints, nHzero), None)
        self.BR_hkhjhi = np.full((nPoints, nHzero, nHzero, nHzero), None)
        self.BR_hjhiZ = np.full((nPoints, nHzero, nHzero), None)
        self.BR_hjemu = np.full((nPoints, nHzero), None)
        self.BR_hjetau = np.full((nPoints, nHzero), None)
        self.BR_hjmutau = np.full((nPoints, nHzero), None)
        self.BR_hjHpiW = np.full((nPoints, nHzero, nHplus), None)
        # HiggsBounds_charged_input
        self.Mhplus = np.full((nPoints, nHplus), None)
        self.GammaTotal_Hpj = np.full((nPoints, nHplus), None)
        self.CS_lep_HpjHmj_ratio = np.full((nPoints, nHplus), None)
        self.BR_tWpb = np.full((nPoints), None)
        self.BR_tHpjb = np.full((nPoints, nHplus), None)
        self.BR_Hpjcs = np.full((nPoints, nHplus), None)
        self.BR_Hpjcb = np.full((nPoints, nHplus), None)
        self.BR_Hpjtaunu = np.full((nPoints, nHplus), None)
        self.BR_Hpjtb = np.full((nPoints, nHplus), None)
        self.BR_HpjWZ = np.full((nPoints, nHplus), None)
        self.BR_HpjhiW = np.full((nPoints, nHplus, nHzero), None)
        # HiggsBounds_charged_input_hadr
        self.CS_Hpjtb = np.full((nPoints, nHplus), None)
        self.CS_Hpjcb = np.full((nPoints, nHplus), None)
        self.CS_Hpjbjet = np.full((nPoints, nHplus), None)
        self.CS_Hpjcjet = np.full((nPoints, nHplus), None)
        self.CS_Hpjjetjet = np.full((nPoints, nHplus), None)
        self.CS_HpjW = np.full((nPoints, nHplus), None)
        self.CS_HpjZ = np.full((nPoints, nHplus), None)
        self.CS_vbf_Hpj = np.full((nPoints, nHplus), None)
        self.CS_HpjHmj = np.full((nPoints, nHplus), None)
        self.CS_Hpjhi = np.full((nPoints, nHplus, nHzero), None)

    def run_full(self):
        for na, v in vars(self).items():
            try:
                if None in v:
                    raise RuntimeError(
                        "Set attribute " + na + " first.")
            except TypeError:
                pass
        try:
            res = hbmixed.call_higgsbounds_full(
                self.mode,
                self.nHzero, self.nHplus, self.nPoints,
                self.Mh, self.GammaTotal, self.CP_value,
                self.dMhneut, self.dMhch,
                self.ghjss_s, self.ghjss_p,
                self.ghjcc_s, self.ghjcc_p,
                self.ghjbb_s, self.ghjbb_p,
                self.ghjtt_s, self.ghjtt_p,
                self.ghjmumu_s, self.ghjmumu_p,
                self.ghjtautau_s, self.ghjtautau_p,
                self.ghjWW, self.ghjZZ, self.ghjZga,
                self.ghjgaga, self.ghjgg, self.ghjhiZ,
                self.BR_hjss, self.BR_hjcc, self.BR_hjbb,
                self.BR_hjtt, self.BR_hjmumu,
                self.BR_hjtautau, self.BR_hjWW,
                self.BR_hjZZ, self.BR_hjZga, self.BR_hjgaga,
                self.BR_hjgg,
                self.BR_hjinvisible, self.BR_hkhjhi,
                self.BR_hjhiZ, self.BR_hjemu, self.BR_hjetau,
                self.BR_hjmutau, self.BR_hjHpiW,
                self.Mhplus, self.GammaTotal_Hpj,
                self.CS_lep_HpjHmj_ratio,
                self.BR_tWpb, self.BR_tHpjb,
                self.BR_Hpjcs, self.BR_Hpjcb,
                self.BR_Hpjtaunu, self.BR_Hpjtb,
                self.BR_HpjWZ, self.BR_HpjhiW,
                self.CS_Hpjtb, self.CS_Hpjcb,
                self.CS_Hpjbjet, self.CS_Hpjcjet,
                self.CS_Hpjjetjet, self.CS_HpjW,
                self.CS_HpjZ, self.CS_vbf_Hpj,
                self.CS_HpjHmj, self.CS_Hpjhi)
            self.HBresult_full = np.array(res[0])
            self.chan_full = np.array(res[1])
            self.obsratio_full = np.array(res[2])
            self.ncombined_full = np.array(res[3])
            # SM-normalized hadronic cross sections
            self.singleH = np.array(res[4])
            self.ggH = np.array(res[5])
            self.bbH = np.array(res[6])
            self.VBF = np.array(res[7])
            self.WH = np.array(res[8])
            self.ZH = np.array(res[9])
            self.ttH = np.array(res[10])
            self.tH_tchan = np.array(res[11])
            self.tH_schan = np.array(res[12])
            self.qqZH = np.array(res[13])
            self.ggZH = np.array(res[14])
        except TypeError as err:
            print("TypeError in HiggsBounds call:")
            print(err)
            print("Not all required arrays were set.")

    def run_hbhs_full(self):
        for na, v in vars(self).items():
            try:
                if None in v:
                    raise RuntimeError(
                        "Set attribute " + na + " first.")
            except TypeError:
                pass
        mh = 125.1
        br = Higgs.BR()
        sm_mode = self.mode
        sm_pdf = self.pdf
        sm_nparam = self.nparam
        sm_Mh = np.array([[mh]])
        sm_GammaTotal = np.array([[br.get_Gam_tot(mh)]])
        sm_dmhneut = self.dMhneut.max()
        sm_BR_hjss = br.get_ss(mh)
        sm_BR_hjcc = br.get_cc(mh)
        sm_BR_hjbb = br.get_bb(mh)
        sm_BR_hjtt = br.get_tt(mh)
        sm_BR_hjmumu = br.get_mm(mh)
        sm_BR_hjtautau = br.get_ll(mh)
        sm_BR_hjWW = br.get_WW(mh)
        sm_BR_hjZZ = br.get_ZZ(mh)
        sm_BR_hjZga = br.get_Zy(mh)
        sm_BR_hjgaga = br.get_yy(mh)
        sm_BR_hjgg = br.get_gg(mh)
        sm_res = hssmhadr.call_sm_higgssignals(
            sm_mode,
            sm_pdf, sm_nparam, sm_Mh, sm_GammaTotal,
            sm_dmhneut,
            sm_BR_hjss, sm_BR_hjcc, sm_BR_hjbb,
            sm_BR_hjtt, sm_BR_hjmumu,
            sm_BR_hjtautau, sm_BR_hjWW, sm_BR_hjZZ,
            sm_BR_hjZga, sm_BR_hjgaga, sm_BR_hjgg)
        self.SM_Chisq_mu = sm_res[0]
        self.SM_Chisq_mh = sm_res[1]
        self.SM_Chisq = sm_res[2]
        self.SM_nobs = sm_res[3]
        self.SM_Pvalue = sm_res[4]
        try:
            res = hbhsmixed.call_hbhs_full(
                self.mode,
                self.nHzero, self.nHplus, self.nPoints,
                self.pdf, self.nparam,
                self.Mh, self.GammaTotal, self.CP_value,
                self.dMhneut, self.dMhch,
                self.ghjss_s, self.ghjss_p,
                self.ghjcc_s, self.ghjcc_p,
                self.ghjbb_s, self.ghjbb_p,
                self.ghjtt_s, self.ghjtt_p,
                self.ghjmumu_s, self.ghjmumu_p,
                self.ghjtautau_s, self.ghjtautau_p,
                self.ghjWW, self.ghjZZ, self.ghjZga,
                self.ghjgaga, self.ghjgg, self.ghjhiZ,
                self.BR_hjss, self.BR_hjcc, self.BR_hjbb,
                self.BR_hjtt, self.BR_hjmumu,
                self.BR_hjtautau, self.BR_hjWW,
                self.BR_hjZZ, self.BR_hjZga, self.BR_hjgaga,
                self.BR_hjgg,
                self.BR_hjinvisible, self.BR_hkhjhi,
                self.BR_hjhiZ, self.BR_hjemu, self.BR_hjetau,
                self.BR_hjmutau, self.BR_hjHpiW,
                self.Mhplus, self.GammaTotal_Hpj,
                self.CS_lep_HpjHmj_ratio,
                self.BR_tWpb, self.BR_tHpjb,
                self.BR_Hpjcs, self.BR_Hpjcb,
                self.BR_Hpjtaunu, self.BR_Hpjtb,
                self.BR_HpjWZ, self.BR_HpjhiW,
                self.CS_Hpjtb, self.CS_Hpjcb,
                self.CS_Hpjbjet, self.CS_Hpjcjet,
                self.CS_Hpjjetjet, self.CS_HpjW,
                self.CS_HpjZ, self.CS_vbf_Hpj,
                self.CS_HpjHmj, self.CS_Hpjhi)
            self.HBresult_full = np.array(res[0])
            self.chan_full = np.array(res[1])
            self.obsratio_full = np.array(res[2])
            self.ncombined_full = np.array(res[3])
            self.Chisq_mu = np.array(res[4])
            self.Chisq_mh = np.array(res[5])
            self.Chisq = np.array(res[6])
            self.nobs = np.array(res[7])
            self.Pvalue = np.array(res[8])
            # Delta Chisqs compared to SM
            self.Delta_Chisq_mu = self.Chisq_mu-self.SM_Chisq_mu
            self.Delta_Chisq_mh = self.Chisq_mh-self.SM_Chisq_mh
            self.Delta_Chisq = self.Chisq-self.SM_Chisq
            # SM-normalized hadronic cross sections
            self.singleH = np.array(res[9])
            self.ggH = np.array(res[10])
            self.bbH = np.array(res[11])
            self.VBF = np.array(res[12])
            self.WH = np.array(res[13])
            self.ZH = np.array(res[14])
            self.ttH = np.array(res[15])
            self.tH_tchan = np.array(res[16])
            self.tH_schan = np.array(res[17])
            self.qqZH = np.array(res[18])
            self.ggZH = np.array(res[19])
        except TypeError as err:
            print("TypeError in HiggsBounds call:")
            print(err)
            print("Not all required arrays were set.")


class HS_mixed(HB):

    def __init__(self, nHzero, nHplus, mode, nPoints):
        super().__init__(nHzero, nHplus, mode, nPoints)
        # For HiggsSignals
        self.pdf = np.full((nPoints), None)
        self.nparam = np.full((nPoints), None)
        # HiggsBounds_neutral_input_effC
        self.ghjss_s = np.full((nPoints, nHzero), None)
        self.ghjss_p = np.full((nPoints, nHzero), None)
        self.ghjcc_s = np.full((nPoints, nHzero), None)
        self.ghjcc_p = np.full((nPoints, nHzero), None)
        self.ghjbb_s = np.full((nPoints, nHzero), None)
        self.ghjbb_p = np.full((nPoints, nHzero), None)
        self.ghjtt_s = np.full((nPoints, nHzero), None)
        self.ghjtt_p = np.full((nPoints, nHzero), None)
        self.ghjmumu_s = np.full((nPoints, nHzero), None)
        self.ghjmumu_p = np.full((nPoints, nHzero), None)
        self.ghjtautau_s = np.full((nPoints, nHzero), None)
        self.ghjtautau_p = np.full((nPoints, nHzero), None)
        self.ghjWW = np.full((nPoints, nHzero), None)
        self.ghjZZ = np.full((nPoints, nHzero), None)
        self.ghjZga = np.full((nPoints, nHzero), None)
        self.ghjgaga = np.full((nPoints, nHzero), None)
        self.ghjgg = np.full((nPoints, nHzero), None)
        self.ghjhiZ = np.full((nPoints, nHzero, nHzero), None)
        # HiggsBounds_neutral_input_SMBR
        self.BR_hjss = np.full((nPoints, nHzero), None)
        self.BR_hjcc = np.full((nPoints, nHzero), None)
        self.BR_hjbb = np.full((nPoints, nHzero), None)
        self.BR_hjtt = np.full((nPoints, nHzero), None)
        self.BR_hjmumu = np.full((nPoints, nHzero), None)
        self.BR_hjtautau = np.full((nPoints, nHzero), None)
        self.BR_hjWW = np.full((nPoints, nHzero), None)
        self.BR_hjZZ = np.full((nPoints, nHzero), None)
        self.BR_hjZga = np.full((nPoints, nHzero), None)
        self.BR_hjgaga = np.full((nPoints, nHzero), None)
        self.BR_hjgg = np.full((nPoints, nHzero), None)
        # HiggsBounds_neutral_input_nonSMBR
        self.BR_hjinvisible = np.full((nPoints, nHzero), None)
        self.BR_hkhjhi = np.full((nPoints, nHzero, nHzero, nHzero), None)
        self.BR_hjhiZ = np.full((nPoints, nHzero, nHzero), None)
        self.BR_hjemu = np.full((nPoints, nHzero), None)
        self.BR_hjetau = np.full((nPoints, nHzero), None)
        self.BR_hjmutau = np.full((nPoints, nHzero), None)
        self.BR_hjHpiW = np.full((nPoints, nHzero, nHplus), None)
        # HiggsBounds_charged_input
        self.Mhplus = np.full((nPoints, nHplus), None)
        self.GammaTotal_Hpj = np.full((nPoints, nHplus), None)
        self.CS_lep_HpjHmj_ratio = np.full((nPoints, nHplus), None)
        self.BR_tWpb = np.full((nPoints), None)
        self.BR_tHpjb = np.full((nPoints, nHplus), None)
        self.BR_Hpjcs = np.full((nPoints, nHplus), None)
        self.BR_Hpjcb = np.full((nPoints, nHplus), None)
        self.BR_Hpjtaunu = np.full((nPoints, nHplus), None)
        self.BR_Hpjtb = np.full((nPoints, nHplus), None)
        self.BR_HpjWZ = np.full((nPoints, nHplus), None)
        self.BR_HpjhiW = np.full((nPoints, nHplus, nHzero), None)

    def run_hs_full(self):
        for na, v in vars(self).items():
            try:
                if None in v:
                    raise RuntimeError(
                        "Set attribute " + na + " first.")
            except TypeError:
                pass
        mh = 125.1
        br = Higgs.BR()
        sm_mode = self.mode
        sm_pdf = self.pdf
        sm_nparam = self.nparam
        sm_Mh = np.array([[mh]])
        sm_GammaTotal = np.array([[br.get_Gam_tot(mh)]])
        sm_dmhneut = self.dMhneut.max()
        sm_BR_hjss = br.get_ss(mh)
        sm_BR_hjcc = br.get_cc(mh)
        sm_BR_hjbb = br.get_bb(mh)
        sm_BR_hjtt = br.get_tt(mh)
        sm_BR_hjmumu = br.get_mm(mh)
        sm_BR_hjtautau = br.get_ll(mh)
        sm_BR_hjWW = br.get_WW(mh)
        sm_BR_hjZZ = br.get_ZZ(mh)
        sm_BR_hjZga = br.get_Zy(mh)
        sm_BR_hjgaga = br.get_yy(mh)
        sm_BR_hjgg = br.get_gg(mh)
        sm_res = hssmhadr.call_sm_higgssignals(
            sm_mode,
            sm_pdf, sm_nparam, sm_Mh, sm_GammaTotal,
            sm_dmhneut,
            sm_BR_hjss, sm_BR_hjcc, sm_BR_hjbb,
            sm_BR_hjtt, sm_BR_hjmumu,
            sm_BR_hjtautau, sm_BR_hjWW, sm_BR_hjZZ,
            sm_BR_hjZga, sm_BR_hjgaga, sm_BR_hjgg)
        self.SM_Chisq_mu = sm_res[0]
        self.SM_Chisq_mh = sm_res[1]
        self.SM_Chisq = sm_res[2]
        self.SM_nobs = sm_res[3]
        self.SM_Pvalue = sm_res[4]
        try:
            res = hsmixed.call_hs_full(
                self.mode,
                self.nHzero, self.nHplus, self.nPoints,
                self.pdf, self.nparam,
                self.Mh, self.GammaTotal, self.CP_value,
                self.dMhneut, self.dMhch,
                self.ghjss_s, self.ghjss_p,
                self.ghjcc_s, self.ghjcc_p,
                self.ghjbb_s, self.ghjbb_p,
                self.ghjtt_s, self.ghjtt_p,
                self.ghjmumu_s, self.ghjmumu_p,
                self.ghjtautau_s, self.ghjtautau_p,
                self.ghjWW, self.ghjZZ, self.ghjZga,
                self.ghjgaga, self.ghjgg, self.ghjhiZ,
                self.BR_hjss, self.BR_hjcc, self.BR_hjbb,
                self.BR_hjtt, self.BR_hjmumu,
                self.BR_hjtautau, self.BR_hjWW,
                self.BR_hjZZ, self.BR_hjZga, self.BR_hjgaga,
                self.BR_hjgg,
                self.BR_hjinvisible, self.BR_hkhjhi,
                self.BR_hjhiZ, self.BR_hjemu, self.BR_hjetau,
                self.BR_hjmutau, self.BR_hjHpiW,
                self.Mhplus, self.GammaTotal_Hpj,
                self.CS_lep_HpjHmj_ratio,
                self.BR_tWpb, self.BR_tHpjb,
                self.BR_Hpjcs, self.BR_Hpjcb,
                self.BR_Hpjtaunu, self.BR_Hpjtb,
                self.BR_HpjWZ, self.BR_HpjhiW)
            self.Chisq_mu = np.array(res[0])
            self.Chisq_mh = np.array(res[1])
            self.Chisq = np.array(res[2])
            self.nobs = np.array(res[3])
            self.Pvalue = np.array(res[4])
            # Delta Chisqs compared to SM
            self.Delta_Chisq_mu = self.Chisq_mu - self.SM_Chisq_mu
            self.Delta_Chisq_mh = self.Chisq_mh - self.SM_Chisq_mh
            self.Delta_Chisq = self.Chisq - self.SM_Chisq
            # SM-normalized hadronic cross sections
            self.singleH = np.array(res[5])
            self.ggH = np.array(res[6])
            self.bbH = np.array(res[7])
            self.VBF = np.array(res[8])
            self.WH = np.array(res[9])
            self.ZH = np.array(res[10])
            self.ttH = np.array(res[11])
            self.tH_tchan = np.array(res[12])
            self.tH_schan = np.array(res[13])
            self.qqZH = np.array(res[14])
            self.ggZH = np.array(res[15])
        except TypeError as err:
            print("TypeError in HiggsBounds call:")
            print(err)
            print("Not all required arrays were set.")
