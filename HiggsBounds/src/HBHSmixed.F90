module hbhsmixed

implicit none

contains


subroutine call_hbhs_full(mode,  &
  nhzero, nhplus, npoints,  &
  pdf, nparam,  &
  mh, gammatotal, cp_value,  &
  dmhneut, dmhch,  &
  ghjss_s, ghjss_p,  &
  ghjcc_s, ghjcc_p,  &
  ghjbb_s, ghjbb_p,  &
  ghjtt_s, ghjtt_p,  &
  ghjmumu_s, ghjmumu_p,  &
  ghjtautau_s, ghjtautau_p,  &
  ghjWW, ghjZZ, ghjZga,  &
  ghjgaga, ghjgg, ghjhiZ,  &
  BR_hjss, BR_hjcc, BR_hjbb,  &
  BR_hjtt, BR_hjmumu,  &
  BR_hjtautau, BR_hjWW,  &
  BR_hjZZ, BR_hjZga, BR_hjgaga,  &
  BR_hjgg,  &
  BR_hjinvisible, BR_hkhjhi,  &
  BR_hjhiZ, BR_hjemu, BR_hjetau,  &
  BR_hjmutau, BR_hjHpiW,  &
  Mhplus, GammaTotal_Hpj,  &
  CS_lep_HpjHmj_ratio,  &
  BR_tWpb, BR_tHpjb,  &
  BR_Hpjcs, BR_Hpjcb,  &
  BR_Hpjtaunu, BR_Hpjtb,  &
  BR_HpjWZ, BR_HpjhiW,  &
  CS_Hpjtb, CS_Hpjcb,  &
  CS_Hpjbjet, CS_Hpjcjet,  &
  CS_Hpjjetjet, CS_HpjW,  &
  CS_HpjZ, CS_vbf_Hpj,  &
  CS_HpjHmj, CS_Hpjhi,  &
  HBresult, chan, obsratio, ncombined,  &
  Chisq_mu, Chisq_mh, Chisq,  &
  nobs, Pvalue,  &
  singleH, ggH, bbH,  &
  VBF, WH, ZH,  &
  ttH, tH_tchan, tH_schan,  &
  qqZH, ggZH)

  ! Which experimental data: mode = 0 -> LandH
  integer, intent(in) :: mode
  ! Input for initialize_HiggsBounds
  integer, intent(in) :: nhzero, nhplus, npoints
  ! Input for HiggsSignals
  integer, intent(in) :: pdf, nparam
  ! Input for HiggsBounds_neutral_input_properties
  real(8), intent(in) :: mh(:,:), gammatotal(:,:), cp_value(:,:)
  ! Input for HiggsBounds_set_mass_uncertainties
  real(8), intent(in) :: dmhneut(:,:), dmhch(:,:)
  ! Input for HiggsBounds_neutral_input_effC
  real(8), intent(in) :: ghjss_s(:,:), ghjss_p(:,:)
  real(8), intent(in) :: ghjcc_s(:,:), ghjcc_p(:,:)
  real(8), intent(in) :: ghjbb_s(:,:), ghjbb_p(:,:)
  real(8), intent(in) :: ghjtt_s(:,:), ghjtt_p(:,:)
  real(8), intent(in) :: ghjmumu_s(:,:), ghjmumu_p(:,:)
  real(8), intent(in) :: ghjtautau_s(:,:), ghjtautau_p(:,:)
  real(8), intent(in) :: ghjWW(:,:), ghjZZ(:,:), ghjZga(:,:)
  real(8), intent(in) :: ghjgaga(:,:), ghjgg(:,:), ghjhiZ(:,:,:)
  ! HiggsBounds_neutral_input_SMBR
  real(8), intent(in) :: BR_hjss(:,:), BR_hjcc(:,:), BR_hjbb(:,:)
  real(8), intent(in) :: BR_hjtt(:,:), BR_hjmumu(:,:)
  real(8), intent(in) :: BR_hjtautau(:,:), BR_hjWW(:,:)
  real(8), intent(in) :: BR_hjZZ(:,:), BR_hjZga(:,:), BR_hjgaga(:,:)
  real(8), intent(in) :: BR_hjgg(:,:)
  ! Input for HiggsBounds_neutral_input_nonSMBR
  real(8), intent(in) :: BR_hjinvisible(:,:), BR_hkhjhi(:,:,:,:)
  real(8), intent(in) :: BR_hjhiZ(:,:,:), BR_hjemu(:,:), BR_hjetau(:,:)
  real(8), intent(in) :: BR_hjmutau(:,:), BR_hjHpiW(:,:,:)
  ! Input for HiggsBounds_charged_input
  real(8), intent(in) :: Mhplus(:,:), GammaTotal_Hpj(:,:)
  real(8), intent(in) :: CS_lep_HpjHmj_ratio(:,:)
  real(8), intent(in) :: BR_tWpb(:), BR_tHpjb(:,:)
  real(8), intent(in) :: BR_Hpjcs(:,:), BR_Hpjcb(:,:)
  real(8), intent(in) :: BR_Hpjtaunu(:,:), BR_Hpjtb(:,:)
  real(8), intent(in) :: BR_HpjWZ(:,:), BR_HpjhiW(:,:,:)
  ! Input for HiggsBounds_charged_input_hadr
  real(8), intent(in) :: CS_Hpjtb(:,:), CS_Hpjcb(:,:)
  real(8), intent(in) :: CS_Hpjbjet(:,:), CS_Hpjcjet(:,:)
  real(8), intent(in) :: CS_Hpjjetjet(:,:), CS_HpjW(:,:)
  real(8), intent(in) :: CS_HpjZ(:,:), CS_vbf_Hpj(:,:)
  real(8), intent(in) :: CS_HpjHmj(:,:), CS_Hpjhi(:,:,:)
  ! Output of run_HiggsBounds_full
  integer :: HBresult(npoints,nhzero+nhplus+1)
  integer :: chan(npoints,nhzero+nhplus+1)
  real(8) :: obsratio(npoints,nhzero+nhplus+1)
  integer :: ncombined(npoints,nhzero+nhplus+1)
  ! Output of run_HiggsSignals
  real(8) :: Chisq_mu(npoints), Chisq_mh(npoints), Chisq(npoints)
  integer :: nobs(npoints)
  real(8) :: Pvalue(npoints)
  ! Cross sections alculated by HiggsBounds
  real(8) :: singleH(npoints,nhzero,4)
  real(8) :: ggH(npoints,nhzero,4)
  real(8) :: bbH(npoints,nhzero,4)
  real(8) :: VBF(npoints,nhzero,4)
  real(8) :: WH(npoints,nhzero,4)
  real(8) :: ZH(npoints,nhzero,4)
  real(8) :: ttH(npoints,nhzero,4)
  real(8) :: tH_tchan(npoints,nhzero,4)
  real(8) :: tH_schan(npoints,nhzero,4)
  real(8) :: qqZH(npoints,nhzero,4)
  real(8) :: ggZH(npoints,nhzero,4)

  integer :: i, j, k

  ! Different colliders and COM energies
  integer, dimension(4) :: col

  !f2py intent(in) :: mode
  !f2py intent(in) :: nhzero, nhplus, npoints
  !f2py intent(in) :: pdf, nparam
  !f2py intent(in) :: mh, dmh, gammatotal
  !f2py intent(in) :: dmhneut, dmhch
  !f2py intent(in) :: ghjss_s, ghjss_p
  !f2py intent(in) :: ghjcc_s, ghjcc_p
  !f2py intent(in) :: ghjbb_s, ghjbb_p
  !f2py intent(in) :: ghjtt_s, ghjtt_p
  !f2py intent(in) :: ghjmumu_s, ghjmumu_p
  !f2py intent(in) :: ghjtautau_s, ghjtautau_p
  !f2py intent(in) :: ghjWW, ghjZZ, ghjZga
  !f2py intent(in) :: ghjgaga, ghjgg, ghjhiZ
  !f2py intent(in) :: BR_hjss, BR_hjcc, BR_hjbb
  !f2py intent(in) :: BR_hjtt, BR_hjmumu
  !f2py intent(in) :: BR_hjtautau, BR_hjWW
  !f2py intent(in) :: BR_hjZZ, BR_hjZga, BR_hjgaga
  !f2py intent(in) :: BR_hjgg
  !f2py intent(in) :: BR_hjinvisible, BR_hkhjhi
  !f2py intent(in) :: BR_hjhiZ, BR_hjemu, BR_hjetau
  !f2py intent(in) :: BR_hjmutau, BR_hjHpiW
  !f2py intent(in) :: BR_tWpb, BR_tHpjb
  !f2py intent(in) :: BR_Hpjcs, BR_Hpjcb
  !f2py intent(in) :: BR_Hpjtaunu, BR_Hpjtb
  !f2py intent(in) :: BR_HpjWZ, BR_HpjhiW
  !f2py intent(in) :: CS_Hpjtb, CS_Hpjcb
  !f2py intent(in) :: CS_Hpjbjet, CS_Hpjcjet
  !f2py intent(in) :: CS_Hpjjetjet, CS_HpjW
  !f2py intent(in) :: CS_HpjZ, CS_vbf_Hpj
  !f2py intent(in) :: CS_HpjHmj, CS_Hpjhi
  !f2py intent(out) :: HBresult, chan
  !f2py intent(out) :: obsratio, ncombined
  !f2py intent(out) :: Chisq_mu, Chisq_mh, Chisq
  !f2py intent(out) :: nobs, Pvalue
  !f2py intent(out) :: singleH, ggH, bbH
  !f2py intent(out) :: VBF, WH, ZH
  !f2py intent(out) :: ttH, tH_tchan, tH_schan
  !f2py intent(out) :: qqZH, ggZH

  col(1) = 2
  col(2) = 7
  col(3) = 8
  col(4) = 13

  if (mode.eq.0) then
    call initialize_HiggsBounds(nhzero, nhplus, "LandH")
    call initialize_HiggsSignals_latestresults(nhzero, nhplus)
  endif

  call setup_output_level(0)

  call setup_pdf(pdf)

  call setup_Nparam(nparam)

  do i=1,npoints

    call HiggsBounds_neutral_input_properties(  &
      mh(i,:),  &
      gammatotal(i,:),  &
      cp_value(i,:))

    call HiggsBounds_set_mass_uncertainties(  &
      dmhneut(i,:),  &
      dmhch(i,:))
    
    call HiggsSignals_neutral_input_MassUncertainty(dmhneut(i,:))

    call HiggsBounds_neutral_input_effC(  &
      ghjss_s(i,:), ghjss_p(i,:),  &
      ghjcc_s(i,:), ghjcc_p(i,:),  &
      ghjbb_s(i,:), ghjbb_p(i,:),  &
      ghjtt_s(i,:), ghjtt_p(i,:),  &
      ghjmumu_s(i,:), ghjmumu_p(i,:),  &
      ghjtautau_s(i,:), ghjtautau_p(i,:),  &
      ghjWW(i,:), ghjZZ(i,:), ghjZga(i,:),  &
      ghjgaga(i,:), ghjgg(i,:), ghjhiZ(i,:,:))

    call HiggsBounds_neutral_input_SMBR(  &
      BR_hjss(i,:), BR_hjcc(i,:), BR_hjbb(i,:), &
      BR_hjtt(i,:), BR_hjmumu(i,:), &
      BR_hjtautau(i,:), BR_hjWW(i,:), &
      BR_hjZZ(i,:), BR_hjZga(i,:), BR_hjgaga(i,:), &
      BR_hjgg(i,:))

    call HiggsBounds_neutral_input_nonSMBR(  &
      BR_hjinvisible(i,:), BR_hkhjhi(i,:,:,:), &
      BR_hjhiZ(i,:,:), BR_hjemu(i,:), BR_hjetau(i,:),  &
      BR_hjmutau(i,:), BR_hjHpiW(i,:,:))

    call HiggsBounds_charged_input(  &
      Mhplus(i,:), GammaTotal_Hpj(i,:), &
      CS_lep_HpjHmj_ratio(i,:), &
      BR_tWpb(i), BR_tHpjb(i,:), &
      BR_Hpjcs(i,:), BR_Hpjcb(i,:),  &
      BR_Hpjtaunu(i,:), BR_Hpjtb(i,:),  &
      BR_HpjWZ(i,:), BR_HpjhiW(i,:,:))

    call HiggsBounds_charged_input_hadr(  &
      col(4), CS_Hpjtb(i,:), CS_Hpjcb(i,:), &
      CS_Hpjbjet(i,:), CS_Hpjcjet(i,:),  &
      CS_Hpjjetjet(i,:), CS_HpjW(i,:),  &
      CS_HpjZ(i,:), CS_vbf_Hpj(i,:),  &
      CS_HpjHmj(i,:), CS_Hpjhi(i,:,:))

    call run_HiggsBounds_full(  &
      HBresult(i,:), chan(i,:), obsratio(i,:), ncombined(i,:))

    call run_HiggsSignals_full(  &
      Chisq_mu(i), Chisq_mh(i), Chisq(i), nobs(i), Pvalue(i))

    do j=1,nhzero
      do k=1,4
        call higgsbounds_get_neutral_hadr_cs(  &
          j, col(k),  &
          singleH(i,j,k), ggH(i,j,k), bbH(i,j,k),  &
          VBF(i,j,k), WH(i,j,k), ZH(i,j,k),  &
          ttH(i,j,k), tH_tchan(i,j,k), tH_schan(i,j,k),  &
          qqZH(i,j,k), ggZH(i,j,k))
      enddo
    enddo
  
  enddo

  call finish_HiggsBounds

  call finish_HiggsSignals

end subroutine call_hbhs_full



end module
