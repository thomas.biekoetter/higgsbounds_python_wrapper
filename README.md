# HiggsBounds

HiggsBounds is a python package that wraps some
functionalities of the HiggsBounds and the HiggsSignals
library to be able to use it directly in python.

So far, only a mixed input format is supported.
The production cross sections are derived internally
by HiggsBounds from the effective coupling input.
The branching ratios, however, have to be given
additionally. This input format corresponds to
the way HiggsBounds treats input from SLHA files.

# Citation guide

Please consider citing the following reference in
case you use the HiggsBounds wrapper in a
scientific publication:

  - **munuSSM: A python package for the μ-from-ν
    Supersymmetric Standard Model**  
    T. Biekötter (Hamburg, Deutsches Elektronen-Synchrotron (DESY)),
    [arXiv:2009.12887]

If you use this package, you are obviously using HiggsBounds
and HiggsSignals. Thus, you should also cite the corresponding
references. Please visit the
[HiggsBounds](https://gitlab.com/higgsbounds/higgsbounds)
and the
[HiggsSignals](https://gitlab.com/higgsbounds/higgssignals)
repositories for further instructions.

Finally, please don't forget to cite the publications
of the experimental searches that are relevant for
your studies.

# Prerequisites

Before the package can be installed, you obviously
need to install HiggsBounds v.5 and HiggsSignals v.2
first. I highly recommend to use the most recent
versions.

To install HiggsBounds, you should do:
```
git clone git@gitlab.com:higgsbounds/higgsbounds.git
cd higgsbounds
mkdir build && cd build
cmake .. -DCMAKE_Fortran_FLAGS="-fPIC"
make
```
If you already have a version of HiggsBounds installed,
it is enough to recompile with:
```
cmake .. -DCMAKE_Fortran_FLAGS="-fPIC"
make
```

To install HiggsSignals, you should do:
```
git clone git@gitlab.com:higgsbounds/higgssignals.git
cd higgssignals
mkdir build && cd build
cmake .. -DCMAKE_Fortran_FLAGS="-fPIC"
make
```
If you already have a version of HiggsSignals installed,
it is enough to recompile with:
```
cmake .. -DCMAKE_Fortran_FLAGS="-fPIC"
make
```

Finally, you need to install the python package
`StandardModel`. Please go to the
[StandardModel](https://gitlab.com/thomas.biekoetter/standard_model)
repository and follow the instructions.

# Installation

Once you have successfully installed HiggsBounds and
HiggsSignals, you can install the python package.
It is required to provide the path
of the HiggsBounds library `libHB.a` and the
HiggsSignals library `libHS.a`. To do that, just
change the variables `HIGGSBOUNDS_LIB_PATH` and
`HIGGSSIGNALS_LIB_PATH` in line 6 and 8 of
the file `setup.py`.

You can find the libraries in
the folders `build/lib` within the corresponding
installation of HiggsBounds and HiggsSignals.

Once you have done that, the package can be installed by doing:
```
python setup.py install --user
```

# Usage

The user has to provide the required input. You can
have a look in the HiggsBounds library for the
complete list of obersvables. They have to be given
as numpy arrays. The shape of the arrays is given by
the shape of the fortran arrays as needed by HiggsBounds
plus an additional first index which runs over the
parameter points you want to evaluate.

The first thing you have to do is to import
the module:
```
import numpy as np
from HiggsBounds.HiggsBounds_Wrapper import HB_mixed
```
Next, you can create an instance of HB_mixed
with the numper of points, the number of neutral
Higgs bosons and the number of charged
Higgs bosons as input:
```
hb = HB_mixed(nHzero, nHplus, "LandH", nPoints)
```
The argument `LandH` defines the experiment that
HiggsBounds will take into account. So far, only
`LandH` is supported. The above command will
initialize the required input arrays, but filled
with `None`. You can have a look into the
example script provided for the complete list
of input arrays. There definition is the same
as stated in the HiggsBounds manual.

It is important to not change the shapes of
the arrays. Otherwise, this will break the code.

Often, some of the input arrays are not relevant,
such that the elements can be set to zero.
The quickest way to do this is by writin,
for instance:
```
hb.BR_hjinvisible = np.full_like(hb.BR_hjinvisible, 0.0)
```

## Running HiggsBounds

Once you have filled all input arrays, you can
call the HiggsBounds routine:
```
hb.run_full()
```
Depending on the number of points, this may take
a while. If you forgot to replace a `None` of
one of the input arrays, you will get an error
at this point. The results of the HiggsBounds
evaluation are saved in:
```
hb.HBresult_full
hb.chan_full
hb.obsratio_full
hb.ncombined_full
```
Again, the definitions are identical to the
original HiggsBounds definitions. However,
the arrays have an additional first index
running through the parameter points.

## Running HiggsBounds and HiggsSignals

If you want to call HiggsBounds and
HiggsSignals, you have to call:
```
hb.run_hbhs_full()
```
In addition, this will give you the
following results:
```
hb.Chisq_mu
hb.Chisq_mh
hb.Chisq
hb.nobs
hb.Pvalue
```
The definitions are identical to the
original HiggsSignals definitions plus
the additional array running through
the points.

For easier interpretation, the routine
also calculates the same quantities
for the Standard Model, and gives
the difference of the chi squareds
w.r.t. the Standard Model in the
following arrays:
```
hb.Delta_Chisq_mu
hb.Delta_Chisq_mh
hb.Delta_Chisq
```

<!-- Links -->
[arXiv:2009.12887]: https://arxiv.org/abs/2009.12887
